﻿using Discord;

namespace test_bot_interfase
{
    public class Buttons
    {
        private ComponentBuilder headMenu= new ComponentBuilder();
        
        SelectMenuBuilder menuBuilder = new SelectMenuBuilder();
            

        public ComponentBuilder CreateMenu()
        { //📲 ⏪ ▶️ ⏩ ⏹️ 🔀 🔁 🔂  ➡️🗒️🔖🖊️
            headMenu = new ComponentBuilder();;
            headMenu = headMenu.WithButton("+", "add", ButtonStyle.Secondary); // add back play next stop mix replayAll replayOne noReplay
            headMenu = headMenu.WithButton("⏪️", "back", ButtonStyle.Secondary);
            headMenu = headMenu.WithButton("⏯️", "play", ButtonStyle.Secondary);
            headMenu = headMenu.WithButton("⏩️", "next", ButtonStyle.Secondary);
            headMenu = headMenu.WithButton("✂️", "clear", ButtonStyle.Secondary, row: 1);
            headMenu = headMenu.WithButton("⏹️", "stop", ButtonStyle.Secondary, row: 1);
            headMenu = headMenu.WithButton("jump", "jump", ButtonStyle.Secondary, row: 1);
            menuBuilder.WithPlaceholder("Выбрать плейлист")
                .WithCustomId("playlist")
                .WithMinValues(1)
                .WithMaxValues(1)
                .AddOption("Настройка плейлиста", "createPlaylist", "Форма для создания/редактирования плейлиста")
                .AddOption("lofii", "pl_1", "Отлично подходит для рабочей атмосферы")
                .AddOption("Рейв", "pl_2", "для рейдов в Genshin");
            headMenu = headMenu.WithSelectMenu(menuBuilder, 2);
            // headMunu = headMunu.WithButton("🔀", "mix", ButtonStyle.Secondary);
            // headMunu = headMunu.WithButton("🔁", "replayAll", ButtonStyle.Secondary);
            // headMunu = headMunu.WithButton("🔂", "replayOne", ButtonStyle.Secondary);
            headMenu = headMenu.WithButton("ON", "testON", ButtonStyle.Primary, row: 2);
            headMenu = headMenu.WithButton("OFF", "testOFF",ButtonStyle.Danger, row: 2);
            /*menuBuilder.WithPlaceholder("Выбрать плейлист")
                .WithCustomId("playlist")
                .WithMinValues(1)
                .WithMaxValues(1)
                .AddOption("Настройка плейлиста", "createPlaylist", "Форма для создания/редактирования плейлиста")
                .AddOption("lofi", "pl_1", "Отлично подходит для рабочей атмосферы")
                .AddOption("Рейв", "pl_2", "для рейдов в Genshin");
            headMunu = headMunu.WithSelectMenu(menuBuilder);*/
            
            
            return headMenu;
            
            
        }
    }
}