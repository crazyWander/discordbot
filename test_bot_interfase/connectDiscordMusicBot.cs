﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using RestSharp;

namespace test_bot_interfase
{
    
    public class ConnectDiscordMusicBot
    {
        private RestClient _restClient = null;
        private RestRequest _request = null;
        public ConnectDiscordMusicBot()
        {
            _restClient = new RestClient("http://localhost:8080/ergo-music-bot-disc");
        }
        public ConnectDiscordMusicBot(string baseURL)
        {
            _restClient = new RestClient(baseURL);
        }
        
        
        
        
        public struct EndPoint
        {
            public const string Join = @"/api/v1/audio/join/{voiceChannelId}";
            public const string Exit = @"/api/v1/audio/exit/{voiceChannelId}/";
            public const string Add = @"/api/v1/track/add";
            public const string Back = @"/api/v1/track/previous";
            public const string Play = @"/api/v1/track/play";
            public const string Next = @"/api/v1/track/next";
            public const string Clear = @"/api/v1/track/clear";
            public const string Stop = @"/api/v1/track/stop";
        }
        
        
        public void RestPost(string endpoint, Dictionary<string,string> urlSegment = null, object parameter = null)
        {
            _request = new RestRequest(endpoint, Method.Post);
            //_request.AddUrlSegment("comand", comand);
            if (urlSegment != null) _request.AddUrlSegment(urlSegment.ElementAt(0).Key, urlSegment[urlSegment.ElementAt(0).Key]);
            if (parameter != null) _request.AddObject(parameter);
            
            _restClient.ExecuteAsync(_request);
            
        }
        
        
        
    }
}