﻿using System.Threading.Tasks;
using Discord;

namespace test_bot_interfase
{
    public class ModalComponent
    {
        private ModalBuilder _addPlay;
        private ModalBuilder _errorBot;
        public ModalBuilder AddMusic()
        {
            if (_addPlay != null)
                return _addPlay;
            else
            _addPlay = new ModalBuilder()
                .WithTitle("Добавить Музыку")
                .WithCustomId("AddMusic")
                .AddTextInput("Введите URL", "url", placeholder:"URL");

            return _addPlay;

            //await Context.Interaction.RespondWithModalAsync(mb.Build());
        }

        public ModalBuilder BotError(string error, string option)
        {
            _errorBot = new ModalBuilder()
                .WithTitle("...Упс, ошибка")
                .WithCustomId("Er")
                .AddTextInput("error",
                    error, TextInputStyle.Paragraph, option, value: option);
                //.AddTextInput(error, "Err", maxLength: 0);
            return _errorBot;
        }
    }
}