﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.Interactions;
using Discord.Net;
using Discord.Rest;
using Discord.WebSocket;


namespace test_bot_interfase
{
    class Program
    {
        static void Main(string[] args)
            => new Program().MainAsync().GetAwaiter().GetResult();

        private DiscordSocketClient client;

        private ModalComponent DiscordForm = new ModalComponent();
        
        private Buttons _buttons = new Buttons();
        ConnectDiscordMusicBot connectAPI = new ConnectDiscordMusicBot();
        private ComponentBuilder menu;
        private async Task MainAsync()
        {
            client = new DiscordSocketClient();
            client.MessageReceived += CommandsHandler; //тригер на получение сообщений
            client.ButtonExecuted += MyButtonHandler;
            client.SelectMenuExecuted += MyMenuHandler;
            client.Log += Log;
            client.SlashCommandExecuted += SlashCommandHandler; // Управление Слеш командами
            client.ModalSubmitted += ModalSub;
            menu = _buttons.CreateMenu();
            
            var token = "";

            await client.LoginAsync(TokenType.Bot, token);//илициализация сессии
            await client.StartAsync();

            Console.ReadLine();
        }

        

            private async Task ModalSub(SocketModal arg)
        {
            List<SocketMessageComponentData> components =
                arg.Data.Components.ToList();
            string urlMusic = null;
			
            foreach (var component in components)
            {
                if (component.CustomId == "url")
                {
                    urlMusic = component.Value;
                    break;
                }
            }
			
			//Написать Bugreport
            var Errors = components
                .Where(x => x.CustomId == "Error");
            if (Errors.Count() > 0)
            {
                string error = Errors.First().Value;
            }

            if (urlMusic != null)
            {
                var url = new {url = urlMusic};
                connectAPI.RestPost(ConnectDiscordMusicBot.EndPoint.Add, null, url);
            }
             
            await arg.RespondAsync();
        }

        
        private async Task SlashCommandHandler(SocketSlashCommand command)
        {
            await command.RespondAsync($"You executed {command.Data.Name}");
        }



        private async Task MyMenuHandler(SocketMessageComponent arg)
        {
			//тест, кнопки
            var text = string.Join(", ", arg.Data.Values);
            if (text == "pl_1")
            {
                var url = new {url = "https://www.youtube.com/watch?v=DU8bzcpULiU"};
                connectAPI.RestPost(ConnectDiscordMusicBot.EndPoint.Add, null, url);
                
                url = new {url = "https://www.youtube.com/watch?v=nyFoLrQGm1I"};
                connectAPI.RestPost(ConnectDiscordMusicBot.EndPoint.Add, null, url);
                
                url = new {url = "https://www.youtube.com/watch?v=Havox-gz4GQ"};
                connectAPI.RestPost(ConnectDiscordMusicBot.EndPoint.Add, null, url);
                
                url = new {url = "https://www.youtube.com/watch?v=8-uBUNUy8SE"};
                connectAPI.RestPost(ConnectDiscordMusicBot.EndPoint.Add, null, url);
            }

            await arg.RespondAsync();
        }

        private async  Task MyButtonHandler(SocketMessageComponent component)
        {
            IVoiceChannel channel;
            switch(component.Data.CustomId)
            {
                case "add":
                    await component.RespondWithModalAsync(DiscordForm.AddMusic().Build());
                    await component.RespondAsync();
                    break;
                case "back":
                    connectAPI.RestPost(ConnectDiscordMusicBot.EndPoint.Back);
                    await component.RespondAsync();
                    break;
                case "play":
                    connectAPI.RestPost(ConnectDiscordMusicBot.EndPoint.Play);
                    await component.RespondAsync();
                    break;
                case "next":
                    connectAPI.RestPost(ConnectDiscordMusicBot.EndPoint.Next);
                    await component.RespondAsync();
                    break;
                case "stop":
                    connectAPI.RestPost(ConnectDiscordMusicBot.EndPoint.Stop);
                    await component.RespondAsync();
                    break;
                case "clear":
                    connectAPI.RestPost(ConnectDiscordMusicBot.EndPoint.Clear);
                    await component.RespondAsync();
                    break;
                case "jump":
                    //connectAPI.RestPost(ConnectDiscordMusicBot.EndPoint.Clear);
                    await component.RespondWithModalAsync(DiscordForm.BotError("Ошибка №501", "Фича пока не создана").Build());
                    break;
                case "testON":
                    channel = null;
                    channel = channel ?? (component.User as IGuildUser)?.VoiceChannel;
                    var dicionary = new Dictionary<string, string>();
                    connectAPI.RestPost(ConnectDiscordMusicBot.EndPoint.Join, 
                        new Dictionary<string, string>() {{"voiceChannelId", channel.Id.ToString()}});
                    await component.RespondAsync();
                    break;
                case "testOFF":
                    channel = null;
                    channel = channel ?? (component.User as IGuildUser)?.VoiceChannel;
                    connectAPI.RestPost(ConnectDiscordMusicBot.EndPoint.Exit,
                        new Dictionary<string, string>(){{"voiceChannelId", channel.Id.ToString()}});
                    await component.Message.DeleteAsync();
                    await component.RespondAsync();
                    break;
                default:
                    await component.RespondAsync($"{component.User.Mention} нажал неработающую кнопку");
                    break;
                
            }
        }
    
        private Task Log(LogMessage msg) //логи
        {
            Console.WriteLine(msg.ToString());
            return Task.CompletedTask;
        }




        private async Task<Task> CommandsHandler(SocketMessage msg)// тут сообщения
        {
            
            if (!msg.Author.IsBot)
            {
                switch (msg.Content)
                {
                    case "!!":
                    {
                        msg.Channel.SendMessageAsync("Основное меню", components: menu.Build());
                        await msg.DeleteAsync();
                        break;
                        }
                    
                    default:
                        {

                            msg.RemoveAllReactionsAsync();
                            break;
                        }
                    
                }
            }
            return Task.CompletedTask;
        }
    }
}